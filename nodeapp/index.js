var express = require('express'),
    http = require('http'),
    redis = require('redis');

var app = express();

console.log(process.env.REDIS_PORT_6379_TCP_ADDR + ':' + process.env.REDIS_PORT_6379_TCP_PORT);
var client = redis.createClient('6379', 'redis');


app.get('/', function(req, res, next) {
  client.incr('counter', function(err, counter) {
    if(err) return next(err);
    res.send('Esta página ha sido visitada ' + counter + ' veces!');
  });
});

http.createServer(app).listen(process.env.PORT || 8080, function() {
  console.log('Escuchando el puerto ' + (process.env.PORT || 8080));
});
